<?php get_header(); ?>

        <p>This  is the single.php file</p>
        <?php
            if (have_posts()) : while (have_posts()) : the_post();
        ?>
        <div class="blog-container text-center auto-margin">
          <h1 class="blog-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
          <div class="author-box auto-margin clearfix">
              <div class="avatar-container clearfix">
                <span class="avatar"><?php echo get_avatar()?></span>
              </div>
              <div class="bio-container clearfix text-left">
                <span class="bio">
                  <p>Written by: <?php the_author(); ?></p>
                  <p class="post-date clearfix"><?php the_date()?></p>
                </span>
              </div>
          </div>
          <div class="post-content"><?php the_content(); ?></div>
          <?php
              endwhile; else:
          ?>
          <p> No more posts to display</p>
          <?php
              endif;
          ?>
        </div>


<?php get_footer(); ?>
