<?php get_header(); ?>

        <p>This  is the home.php file</p>
        <?php
            if (have_posts()) : while (have_posts()) : the_post();
        ?>
        <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
        <p><?php the_content(); ?></p>
        <hr>

        <?php
            endwhile; else:
        ?>
        <p> No more posts to display</p>
        <?php
            endif;
        ?>

<?php get_footer(); ?>
