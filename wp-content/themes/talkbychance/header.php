<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>
            <?php
                wp_title('-',TRUE,'right');
                bloginfo('name');
             ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale = 1.0">
        <?php wp_head(); ?>
    </head>
    <body>
      <div class="container-fluid">

        <nav class="navbar navbar-default" style="background-color">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                <a href="<?php site_url()?>"class="navbar-brand">TalkByChance</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <?php
                  $args = array(
                  'menu' => 'main menu','container' => '','items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>');

                  wp_nav_menu($args);
              ?>
              <form class="navbar-form navbar-right" role="search">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
              </form>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
      </div>
      <!-- Navigation Ends -->
