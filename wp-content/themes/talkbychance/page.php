<?php get_header(); ?>

        <p>This  is the page.php file</p>
        <?php
            if (have_posts()) : while (have_posts()) : the_post();
        ?>
        <h3><?php the_title(); ?></h3>
        <p><?php the_content(); ?></p>
        <hr>

        <?php
            endwhile; else:
        ?>
        <p> No more posts to display</p>
        <?php
            endif;
        ?>
    
<?php get_footer(); ?>