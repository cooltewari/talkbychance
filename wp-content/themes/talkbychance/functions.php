<?php
//Load Theme CSS
function theme_styles() {
  wp_enqueue_style( 'stylemain', get_template_directory_uri().'/style.css');
  wp_enqueue_style( 'bootstrapcss', get_template_directory_uri().'/css/bootstrap.min.css');

  wp_register_style( 'slider-css', get_template_directory_uri().'/css/slider.css');
  if (is_page( 'home' )){
    # code...
    wp_enqueue_style( 'slider-css' );
  }
}
add_action( 'wp_enqueue_scripts','theme_styles' );

//Load Theme JS
function theme_scripts() {
  wp_enqueue_script( 'bootstrapjs', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery'),'',true);
  wp_enqueue_script( 'app', get_template_directory_uri().'/js/app.js', array('jquery'),'',true );
}
add_action( 'wp_enqueue_scripts', 'theme_scripts');
// Enable custom menus
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'extra-menu' => __( 'Extra Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

//custom excerpt length
function wpfme_custom_excerpt_length( $length ) {
	//the amount of words to return
	return 10;
}
add_filter( 'excerpt_length', 'wpfme_custom_excerpt_length');
?>
